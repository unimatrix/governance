# UniMatrix Governance: Analytics Application Management Framework

This Governance Charter sets forth the responsibilities and procedures for non-technical support (the “Governance Project”) of the UniMatrix technical project, which has been established as the UniMatrix Project a Series of LF Projects, LLC (the “Technical Project”).  The Technical Project is an open source project based on established standards for the physical security industry for management of interoperable analytics applications.  Contributors to the Technical Project must comply with the charter of the Technical Project which can be found here https://gitlab.com/unimatrix  Participants in the Governance Project must comply with this Governance Charter as well as any applicable policies of The Linux Foundation https://www.linuxfoundation.org/policies/.

## Mission

The mission of the Governance Project is to support the Technical Project. The Technical Project has following objectives:

* To create an Open Source reference implementation using industry standard APIs and frameworks to allow management of cloud native applications in the physical security industry ecosystem for Analytics in order to enhance interoperability.
* To develop, manage, deploy, orchestrate and run container based applications.

## Governing Board

1. The Governing Board (GB) shall be responsible for overall oversight of the Governance Project.
2. At the inception of the Governance Project, the GB voting members are as follows.
    
    1. Ottavio Campana, o.campana@videotec.com
    2. Nishihara Makoto (西原 誠) <nishihara.makoto@jp.panasonic.com> **Kitaoka, Hirokazu, hirokazu.kitaoka@us.panasonic.com is replaced in GB as of September 2021 and is no longer member of GB.**
    3. Adler Wu, wuhaifeng@hikvision.com
    4. (Zhangyalan (Judith) zhangyalan@huawei.com) **Retired from GB as of Januarty 2021 and is no longer member of GB. Huawei has not been repleced by other member.**
    5. Hugo Brisson, hbrisson@genetec.com
    6. Milan Gada, Milan.Gada@microsoft.com
3. Following the inception of the Project, the GB will implement procedures and methodologies for the selection of GB voting members from the contributing community. The maximum number of seats in GB is initially set to 7 (seven).
4. Each member of GB is expected to follow General Rules and Operations and set a good example for participation. Failure to do so such as repeated absence from GB meetings may result in exclusion. The exclusion of a GB member will be decided by a confidential vote where a majority vote must be in favor of exclusion.
5. The GB will encourage transparency (e.g., publish public minutes). GB meetings will be open to the public, and can be conducted electronically, via teleconference, or in person.
6. TB will communicate with the GB with respect to the activities of the Technical Project, its projects and working groups.
7. The GB shall annually elect a GB Chair and GB Co-Chair from the contributing community involved in either or both the Governing Board and or the technical board (“TB”) of the Technical Project. The Chair and Co-Chair will set the agenda and preside over meetings of the GB.
8. The GB shall actively engage in addressing any violation of this Charter or the Project's Code of Conduct and take corrective actions. The consequence of a violation may lead to a suspension of the account of the member or entity responsible for the violation.
9. Responsibilities: The GB shall also be responsible for:
    * Coordinating the direction of the Governance Project
    * Communicating with external and industry organizations concerning Project matters
    * Appointing representatives to work with other open source or open standards communities
    * Deciding upon matters relating to the support of the UniMatrix community and the Technical Project.
    * Coordinate any marketing, events, or communications with The Linux Foundation.

## GB Voting

1. While it is the goal of the project to operate as a consensus-based community, if any GB decision requires a vote to move forward, the respective voting members shall vote on a one vote per voting member company (including affiliates) basis.
2. Electronic voting may be arranged. Decisions made by electronic vote without a meeting shall require a majority of all voting members of the GB, as applicable.
3. The quorum for GB meetings requires two-thirds of all voting members of the GB. The GB may continue to meet if the quorum is not met but are prevented from making any decisions at the meeting. If the quorum is not met, a second meeting with the same agenda can be called after two weeks to which quorum will become 1/2 of all voting members.
4. Except as provided under Section [Amendments](#Amendments), decisions by vote at a meeting shall require a majority vote of those in attendance, provided quorum is met
5. In the event that any vote cannot be resolved by the GB, any voting member on the GB is entitled to refer the matter to The Linux Foundation for assistance in reaching a decision.

## Antitrust Guidelines

1. All participants in the Project must abide by The Linux Foundation Antitrust Policy available at <http://www.linuxfoundation.org/antitrust-policy>.
2. All participants should encourage open participation from any organization able to meet the participation requirements, regardless of competitive interests. More specifically, the community may not seek to exclude any participant based on any criteria, requirements, or reasons other than those that are reasonable and allied on a non-discriminatory basis to all participants.

## Code of Conduct

1. The GB may adopt a fair, reasonable, and non-discriminatory Project code-of-conduct, with approval from The Linux Foundation as outlined below.
2. The Project will operate in a transparent, open, collaborative, and ethical manner at all times.
3. The output of all Project discussions, proposals, timelines, decisions, and status will be made open and easily visible to all project participants.
4. The Project code of conduct can be found at: https://lfprojects.org/policies/code-of-conduct/.

## Budget and Funding

1. The GB will coordinate any budget or funding needs with The Linux Foundation. Organizations participating may be solicited to sponsor Project activities and infrastructure needs on a voluntary basis.
2. The Project will not raise any funds and will be supported on a volunteer basis by the Project participants.
3. Under no circumstances will The Linux Foundation be expected or required to undertake any action on behalf of the Project that is inconsistent with the tax-exempt purpose of The Linux Foundation.

## General Rules and Operations

The Project and the participants shall:

1. Engage in the work of the Project in a professional manner consistent with maintaining a cohesive community, while also maintaining the goodwill and esteem of The Linux Foundation in the open source software community;
2. Respect the rights of all trademark owners, including  the trademark policy of the Linux Foundation available at https://www.linuxfoundation.org/trademark-usage/ ;
3. Not publicly reference Contributors, Maintainers, their respective employers or other identifiable attributes without prior consent by the subject.
4. Engage the Linux Foundation for all Project press and analyst relations activities;
5. Upon request, provide information regarding Project participation, including information regarding attendance at Project-sponsored events, to The Linux Foundation;
6. Coordinate with The Linux Foundation in relation to any websites created directly for the Project; and
7. Upon reasonable suspicion or knowledge of a violation to this Charter or the Project's Code of Conduct assign the task to the GB to investigate the incident and decide proper action.

## Amendments

This Charter may be amended by a two-thirds vote of the entire GB, subject to approval by The Linux Foundation to ensure the amendment is in-line with non-profit requirements and open source principles.
