# Governance

Files related to governance of the UniMatrix project.

1. [Governance Board Charter](Governance_Board_Charter.md)
1. [Code of Conduct](code_of_conduct.md)
1. [Contributing guidelines](CONTRIBUTING.md)
1. [Technical Board Charter](Technical_Board_Charter.md)
1. [Technical Goals 2020](goals_2020.md)
