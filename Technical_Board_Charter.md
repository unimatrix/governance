# Technical Charter (the “Charter”) for UniMatrix Project a Series of LF Projects, LLC

Adopted ___________

This Charter sets forth the responsibilities and procedures for technical contribution to, and oversight of, the UniMatrix open source project,
which has been established as UniMatrix Project a Series of LF Projects, LLC (the “Project”).  LF Projects, LLC (“LF Projects”) is a Delaware series
limited liability company. All Contributors (including Maintainers, and other technical positions) and other participants in the Project
(collectively, Contributors) must comply with the terms of this Charter.

## 1. Mission and Scope of the Project

* **a.** The mission of the Project is to create an Open Source reference implementation using industry standard APIs and frameworks to allow management of cloud native applications in the physical security industry ecosystem for Analytics in order to enhance interoperability.
* **b.** The scope of the Project includes collaborative development under the Project License (as defined herein) supporting the mission, including documentation, testing, integration and the creation of other artifacts that aid the development, deployment, operation or adoption of the open source project.

## 2. Technical Board

* **a.** The Technical Board (the “TB”) will be responsible for all technical oversight of the open source Project. The TB will have a maximum of seven members (the “TB Cap”). The TB may change the TB Cap by vote.
* **b.** The TB voting members are the Project’s Maintainers. The Maintainers of the Project are as set forth within the “CONTRIBUTING” file within the Project’s code repository. The TB may choose an alternative approach for determining the voting members of the TB, and any such alternative approach will be documented in the CONTRIBUTING file.  Any meetings of the TB are intended to be open to the public, and can be conducted electronically, via teleconference, or in person.
* **c.** TB projects generally will involve Contributors and Maintainers. The TB may adopt or modify roles so long as the roles are documented in the CONTRIBUTING file. Unless otherwise documented:
  * **i.** Contributors include anyone in the technical community that contributes code, documentation, or other technical artifacts to the Project;
  * **ii.** Maintainers are Contributors who have earned the ability to modify (“commit”) source code, documentation or other technical artifacts in a project’s repository; and
  * **iii.**  A Contributor may become a Maintainer by a two third's majority approval of the existing Maintainers. A Maintainer may be removed by a majority approval of the other existing Maintainers.
* **d.** Participation in the Project through becoming a Contributor and Maintainer is open to anyone so long as they abide by the terms of this Charter.
* **e.** The TB may (1) establish work flow procedures for the submission, approval, and closure/archiving of projects, (2) set requirements for the promotion of Contributors to Maintainer status, as applicable, and (3) amend, adjust, refine and/or eliminate the roles of Contributors, and Maintainers, and create new roles, and publicly document any TB roles, as it sees fit.
* **f.** The TB may elect a TB Chair, who will preside over meetings of the TB and will serve until their resignation or replacement by the TB.
* **g.** Responsibilities: The TB will be responsible for all aspects of oversight relating to the Project, which may include:
  * **i.** coordinating the technical direction of the Project;
  * **ii.**  setting specifications (“Specifications”) for development by the Project as sub-projects and approving draft and final Specifications;
  * **iii.** approving project or system proposals (including, but not limited to, incubation, deprecation, and changes to a sub-project’s scope);
  * **iv.** organizing sub-projects and removing sub-projects;
  * **v.** creating sub-committees or working groups to focus on cross-project technical issues and requirements;
  * **vi.** appointing representatives to work with other open source or open standards communities;
  * **vii.** establishing community norms, workflows, issuing releases, and security issue reporting policies;
  * **viii.** approving and implementing policies and processes for contributing (to be published in the CONTRIBUTING file) and coordinating with the series manager of the Project (as provided for in the Series Agreement, the “Series Manager”) to resolve matters or concerns that may arise as set forth in Section 7 of this Charter;
  * **ix.** discussing, seeking consensus, and where necessary, arrange voting on technical matters relating to the code base that affect multiple projects; and
  * **x.** coordinating any marketing, events, or communications regarding the Project.
* **h.** Related Companies
  * **i.** Definitions
    * **1.** “Related Company” means any organization which controls or is controlled by an entity or which, together with an entity, is under the common control of a third party, in each case where such control results from ownership, either directly or indirectly, of more than fifty percent of the voting securities or membership interests of the organization in question; and
    * **2.** “Related Companies” are organizations that are each a Related Company of an entity.
  * **ii.** At no time will two or more TB members be employed by Related Companies.  If, as a result of merger, acquisition or otherwise, more than one TB member is employed by Related Companies, all but one of such members must promptly resign from the TB.

## 3. TB Voting

* **a.** While the Project aims to operate as a consensus-based community, if any TB decision requires a vote to move the Project forward, the voting members of the TB will vote on a one vote per voting member basis.
* **b.** Electronic voting may be arranged. Decisions made by electronic vote without a meeting shall require a majority of all voting members of the TB, as applicable.
* **c.** Quorum for TB meetings requires at least fifty percent of all voting members of the TB to be present. The TB may continue to meet if quorum is not met but will be prevented from making any decisions at the meeting.
* **d.** Except as provided in Sections 3.e, 7.c. and 8.a, decisions by vote at a meeting require a majority vote of those in attendance, provided quorum is met. Decisions made by electronic vote without a meeting require a majority vote of all voting members of the TB.
* **e.** In the event a vote cannot be resolved by the TB, any voting member of the TB may refer the matter to the Series Manager for assistance in reaching a resolution.
* **f.** The TB may add and remove TB members by a two-thirds majority vote.

## 4. Compliance with Policies

* **a.** This Charter is subject to the Series Agreement for the Project and the Operating Agreement of LF Projects. Contributors will comply with the policies of LF Projects as may be adopted and amended by LF Projects, including, without limitation the policies listed at <https://lfprojects.org/policies/>.
* **b.** The GB has adopted a code of conduct (“CoC”) for the Project available at https://lfprojects.org/policies/code-of-conduct/ .
* **c.** When amending or adopting any policy applicable to the Project, LF Projects will publish such policy, as to be amended or adopted, on its web site at least 30 days prior to such policy taking effect; provided, however, that in the case of any amendment of the Trademark Policy or Terms of Use of LF Projects, any such amendment is effective upon publication on LF Project’s web site.
* **d.** All Contributors must allow open participation from any individual or organization meeting the requirements for contributing under this Charter and any policies adopted for all Contributors by the TB, regardless of competitive interests. Put another way, the Project community must not seek to exclude any participant based on any criteria, requirement, or reason other than those that are reasonable and applied on a non-discriminatory basis to all Contributors in the Project community.
* **e.** The Project will operate in a transparent, open, collaborative, and ethical manner at all times. The output of all Project discussions, proposals, timelines, decisions, and status should be made open and easily visible to all. Any potential violations of this requirement should be reported immediately to the Series Manager.

## 5. Community Assets

* **a.** LF Projects will hold title to all trade or service marks used by the Project (“Project Trademarks”), whether based on common law or registered rights.  Any Project Trademarks created by the Project will be transferred and assigned to LF Projects to hold on behalf of the Project. The Project will be entitled to use these Project Trademarks in accordance with the license from LF Projects stipulated in the Series Agreement. Any such use will inure to the benefit of LF Projects.
* **b.** The Project will, as permitted and in accordance with such license from LF Projects, develop and own all Project GitLab and social media accounts, and domain name registrations created by the Project community.
* **c.** Under no circumstances will LF Projects be expected or required to undertake any action on behalf of the Project that is inconsistent with the tax-exempt status or purpose, as applicable, of LFP, Inc. or LF Projects, LLC.

## 6. General Rules and Operations

* **a.** The Project will:
  * **i.** engage in the work of the Project in a professional manner consistent with maintaining a cohesive community, while also maintaining the goodwill and esteem of LF Projects, LFP, Inc. and other partner organizations in the open source community; and
  * **ii.** respect the rights of all trademark owners, including any branding and trademark usage guidelines.

## 7. Intellectual Property Policy

* **a.** Contributors acknowledge that the copyright in all new contributions will be retained by the copyright holder as independent works of authorship and that no Contributors or copyright holder will be required to assign copyrights to the Project.
* **b.** Except as described in Section 7.c., all contributions to the Project are subject to the following:
  * **i.** All new inbound code contributions to the Project must be made using the Apache License, Version 2.0, available at <https://www.apache.org/licenses/LICENSE-2.0> (the “Project License”).
  * **ii.** All new inbound code contributions must also be accompanied by a Developer Certificate of Origin (<http://developercertificate.org>) sign-off in the source code system that is submitted through a TB-approved contribution process which will bind the authorized Contributor and, if not self-employed, their employer to the applicable license;
  * **iii.** All outbound code will be made available under the Project License.
  * **iv.** Specifications will be contributed to and made available by the Project using the Project License.
  * **v.** All documentation with exception of Specifications will be received and made available by the Project under the Creative Commons Attribution 4.0 International License (available at <http://creativecommons.org/licenses/by/4.0/>).
  * **vi.** The Project may seek to integrate and contribute back to other open source projects (“Upstream Projects”). In such cases, the Project will conform to all license requirements of the Upstream Projects, including dependencies, leveraged by the Project.  Upstream Project code contributions not stored within the Project’s main code repository will comply with the contribution process and license terms for the applicable Upstream Project.
* **c.** The TB may approve the use of an alternative license or licenses for inbound or outbound contributions on an exception basis. To request an exception, please describe the contribution, the alternative open source license(s), and the justification for using an alternative open source license for the Project. License exceptions must be approved by a two-thirds vote of the entire TB.
* **d.** Contributed files should contain license information, such as SPDX short form identifiers, indicating the open source license or licenses pertaining to the file.

## 8. Amendments

* **a.** This charter may be amended by a two-thirds vote of the entire TB and is subject to approval by LF Projects.


