# Technical Goals 2020

1. Generic Requirements
1. Video API
1. Inference API
1. Deployment API
1. Audio API
1. Capabilities API
1. Orchestration
1. Common system components

## Generic Requirements

1. Use industry standard APIs where it makes sense
1. Adhere to CNCF principles[^cncf]
1. For single node use-cases, zero-copy transfer/reference should be used for bulk data
1. For network use-cases, efficient binary transport should be used, e.g. gRPC

### Language Support

1. C/C++
1. Python

### Reference Platforms

#### Edge Platform

1. Raspberry Pi 4
1. 64GB SD-card Extreme Pro
1. Camera module

### SDK Packaging Requirements

1. The SDK is packaged as a set of container layers
1. The base layers should allow host to provide hardware specific backends for
   1. Efficient I/O
   1. Hardware acceleration
1. Language bindings should be packaged in different layers
   1. C/C++ as base
   1. Python as extra layer
1. Extra layers for development
   1. Provides full SDK, including examples, documenation, header files, etc

### DevOps Requirements

1. All implementations shall have an automated test suite
1. Define a release process, schedule and recurrence
1. Automate CI/CD

## Video API

### Video API Goals

1. Define a sub-set of OpenCV that realize the Use Cases
1. Implement the API for the Reference Platforms
1. Package the API in a set of base container layers
1. Define a test suite that can verify the Use Cases
1. Implement the test suite for the Reference Platforms

### Video API Use Cases

1. Capture raw video frames
   1. Goal: Define requirements on mandatory video formats
1. Get frame/source properties: timestamp, resolution, video format, fps
   1. Goal: Define other mandatory properties
   1. Goal: Define other optional properties
1. Set frame/source properties: resolution, video format, fps
   1. Goal: Define other mandatory properties
   1. Goal: Define other optional properties
1. Convert color spaces
   1. Goal: Investigate hardware acceleration backends
1. Draw text and graphical overlays on frames
   1. Goal: Investigate hardware acceleration backends

## Inference API

### Inference API Goals

1. Define an inference API according to the Requirements
1. Implement an inference engine according to the Requirements
1. Implement the TensorFlow Serving gRPC API to allow inference using a well-known API that can also be used from other nodes

### Inference API Requirements

1. The API and engine shall allow for zero-copy transfer of tensors
1. The API and engine shall be agnostic to the underlying model format
   1. It shall be possible to support different model formats by implementing different backends
1. The following backends shall be supported on the Reference Edge Platform
   1. TF Lite model to run on CPU
   1. OpenVINO model to run on Intel Movidius NCS
   1. Google Edge TPU model to run on Coral USB accelerator 
1. The API and engine shall be agnostic to the input tensor data format
1. The API and engine shall be agnostic to the output tensor data format

## Deployment API

### Deployment API Goals

1. Implement the containerd gRPC API on the Reference Platforms

## Audio API

### Audio API Goals

1. Investigate different audio frameworks' pros and cons

## Capabilities API

### Capabilities API Goals

1. Define an API that supports the Use Cases

### Capabilities API Use Cases

1. Query supported Imaging capaibilities
1. Query supported PTZ capabilities

## Orchestration

* Not mandatory
* KubeEdge

## Common system components

1. Inference engine
1. Database cache
1. MQTT broker

## Notes

[^cncf]: See <https://github.com/cncf/toc/blob/master/DEFINITION.md>
