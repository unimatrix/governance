# How to contribute

We'd love to accept your patches and contributions to this project. There are just a few small guidelines you should follow.

## Code of conduct

This project follows the guidelines described in [code of conduct](code_of_conduct.md).

## Reporting bugs and feedback

Bugs and feedback are reported through the project's GitLab issue tracker. Anyone is allowed to create issues.

## Code reviews

All submissions, including submissions by project members, require review. We use GitLab merge requests for this purpose. Consult GitLab help for more information on using merge requests. To be able to create a branch to prepare a merge request you must have Developer status in the project. Please apply at the UniMatrix Members page.

* If your contribution is minor, such as a typo fix, just open a merge request.
* If your contribution is major, start by opening an issue first. That way, other people can weigh in on the discussion before you do any work.

## Roles

The [charter](Technical_Board_Charter.md) defines two roles: Contributor and Maintainer. These are mapped to roles in GitLab like this:

* Contributor corresponds to Developer in GitLab.
* Maintainer corresponds to Maintainer in GitLab.

See the [documentation](https://docs.gitlab.com/ee/user/permissions.html) for a description what each role can do in GitLab.

## Developer Certificate of Origin

Contributions to this project must be accompanied by a Developer Certificate of Origin <http://developercertificate.org> sign-off. You (or your employer) retain the copyright to your contribution; this is a certification that, among other statements, you have the right to submit the contribution under the license indicated in the file. Check this [guide](https://git-scm.com/book/en/v2/Git-Tools-Signing-Your-Work) for how to enable automatic signing of your commits.

## Style guides

### Commit messages

* Write clear and meaningful commit messages.
* Use the present tense ("Add feature" not "Added feature").
* Use the imperative mood ("Move cursor to..." not "Moves cursor to...").
* Limit the first line to 72 characters or less.
* Reference issues (#nnn) and merge requests (!nnn) liberally after the first line.

## Maintainers

Listed below are the maintainers that can approve merge requests. They also form the Technical Board of the project as defined by the [charter](Technical_Board_Charter.md).

1. 
1. Manuel Turetta (@manuel.turetta), manuel.turetta@videotec.com
1. Takahiro Yamaguchi (@takahiro777), yamaguchi.takahiro3@jp.panasonic.com
1. Dora Han (@Dora120), hanhaina@hikvision.com
1. Judith Zhang (@Laner_Judith), zhangyalan@huawei.com
1. Sebastien Nadeau (@sebnadeau), snadeau@genetec.com
1. Milan Gada, milan.gada@microsoft.com
